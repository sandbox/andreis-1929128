(function($) {

  Drupal.ajax.prototype.commands.gsc_result = function(ajax, response, status) {
    console.log('return value from server');

    var element = google.search.cse.element.getElement('searchresults-only0');
    if (response.data == '') {
      element.clearAllResults();
    } else {
      element.execute(response.data);
    }
    return false;


  }

  Drupal.ajax.prototype.commands.gsc_result_query = function(ajax, response, status) {
    console.log('return value from server');

    window.location.href = response.data;


  }

}(jQuery));