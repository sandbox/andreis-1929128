(function($) {

  Drupal.behaviors.gsc_init = {
    attach: function (context, settings) {
      $('body', context).once('gsc_init', function () {
        var search_result_container = $('<div></div>',{id : 'gsc_container'});
        $(search_result_container).html('<gcse:searchresults-only></gcse:searchresults-only>');
        $(this).prepend(search_result_container);

      });


    }
  };

}(jQuery));